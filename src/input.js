import React, {Component,Fragment} from 'react';
import "./style.css";
import List from "./list";

/**
 * 父组件，用来修改数据
 */
class Input extends Component {
  constructor(props) {
    super(props);
    //性能优化
    this.empty = this.empty.bind(this);
    this.write = this.write.bind(this);
    this.send = this.send.bind(this);
    this.delete = this.delete.bind(this);
    //初始化数据
    this.state = {
      inputValue:"默认值",
      list:["Java","React"]
    }
  }

  render() {
    return(
        <Fragment>
          <label htmlFor="input">请输入内容：</label>
          {/*为input标签绑定事件*/}
          <input id="input" className="input" value = {this.state.inputValue} onClick={this.empty} onChange={this.write} onKeyUp={this.send}/>
          <ul>
            {/*调用函数*/}
            {this.getList()}
          </ul>
        </Fragment>
    );
  }

  //遍历list数组，并将list数组的值赋值到li标签
  getList(){
    //父组件向子组件传递数据
    return(
        this.state.list.map((value, index) => {
          return <List list={value} key={index} index={index} deleteFunction={this.delete}/>
        })
    )
  }

  //事件
  empty(e){
    this.setState({
      inputValue:""
    })
  };
  write(e){
    this.setState({
      inputValue:e.target.value
    })
  };
  send(e){
    let list = [...this.state.list,e.target.value];
    if(e.keyCode===13){
      if(e.target.value===""){
        alert("数据不能为空！");
      }
      else {
        this.setState({
          list:list,
          inputValue:""
        })
      }
    }
  };
  delete(index){
    let list = [...this.state.list];
    list.splice(index,1);
    this.setState({
      list:list,
    })
  }
}

export default Input;
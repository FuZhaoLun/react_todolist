import React from 'react';
import ReactDOM from 'react-dom';
import Input from './input.js';

ReactDOM.render(
    <Input/>,
    document.getElementById('root')
);

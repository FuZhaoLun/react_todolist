import React, {Component} from 'react';

/**
 * 子组件，用来展示数据
 */
class List extends Component{
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }

    render() {
        //dangerouslySetInnerHTML表示不进行转义
        return(
            <li onClick={this.delete} dangerouslySetInnerHTML={{__html:this.props.list}}></li>
        );
    }

    //事件
    delete(){
        //子组件调用父组件的函数，改变父组件的数据
        const{deleteFunction,index} = this.props;
        deleteFunction(index);
        //上面两句相当于：this.props.deleteFunction(this.props.index);
    }
}

export default List;